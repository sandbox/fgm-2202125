<?php
/**
 * @file
 * views_timeline.views.inc
 *
 * Views integration for Timeline
 *
 * @author: Frederic G. MARAND <fgm@osinet.fr>
 *
 * @copyright (c) 2013-2014 Ouest Systèmes Informatiques (OSInet).
 *
 * @license General Public License version 2 or later
 */

/**
 * Implements hook_views_plugins().
 *
 * @return array
 */
function views_timeline_views_plugins() {
  $module_path = drupal_get_path('module', 'views_timeline');
  $ret = array(
    'module' => 'views_timeline',
    'style' => array(
      'timeline' => array(
        'title' => t('Timeline'),
        'help' => t('Displays rows in a timeline.'),
        'handler' => 'views_timeline_plugin_style_views_timeline',
        'path' => __DIR__,
        'theme' => 'views_view_views_timeline',
        'theme file' => 'theme.inc',
        'theme path' => "$module_path/theme",
        'uses grouping' => FALSE,
        'uses row plugin' => TRUE,
        'uses row class' => FALSE,
        'uses fields' => TRUE,
        'uses options' => TRUE,
        'even empty' => TRUE,
        'type' => 'normal',
        'help topic' => 'style-views-timeline',
      ),
    ),
  );

  return $ret;
}

/**
 * Implements hook_views_default_views().
 *
 * @return array
 */
function views_timeline_views_default_views() {
  $module_path = drupal_get_path('module', 'views_timeline');

  $views = array();

  // Override image path with the current module path.
  require_once __DIR__ . '/views_timeline_example.view.inc';
  $image_path = theme('image', array(
    'path' => "$module_path/theme/user-plain-blue.png",
    'alt' => '[title]',
    'title' => '[title]',
  ));
  $view->display['default']->handler->display->display_options['fields']['nothing']['alter']['text'] = $image_path;

  $views[$view->name] = $view;
  return $views;
}
