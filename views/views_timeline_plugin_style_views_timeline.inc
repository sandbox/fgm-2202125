<?php
/**
 * @file
 * views_timeline_style_views_timeline.inc
 *
 * Contains the Timeline style plugin
 *
 * @author: Frederic G. MARAND <fgm@osinet.fr>
 *
 * @copyright (c) 2013-2014 Ouest Systèmes Informatiques (OSInet).
 *
 * @license General Public License version 2 or later
 */
use OSInet\ViewsTimeline\TimeSpanSelector;

/**
 * Style plugin to render each item as a positioned image in a list
 *
 * @ingroup views_style_plugins
 */
class views_timeline_plugin_style_views_timeline extends views_plugin_style_list {
  /**
   * @var OSInet\ViewsTimeline\TimeSpanSelector
   */
  protected $timeSpanSelector;

  /**
   * @constructor
   */
  function __construct()  {
    // No injected services on D7, so create dependencies in constructor.
    $this->timeSpanSelector = new TimeSpanSelector(REQUEST_TIME);
  }

  /**
   * Set default options
   */
  function option_definition() {
    $options = parent::option_definition();
    $options['datefield'] = array();
    $options['timespan'] = array('default' => $this->timeSpanSelector->getOptionsDefault());
    return $options;
  }

  /**
   * @param array $form
   * @param array $form_state
   */
  public function options_form(&$form, &$form_state) {
    // Bring in List options
    parent::options_form($form, $form_state);
    $labels = $this->display->handler->get_field_labels();
    $form['datefield'] = array(
      '#title' => t('Field holding the timeline date'),
      '#type' => 'select',
      '#options' => $labels,
      '#default_value' => $this->options['datefield'],
    );
    $form['timespan'] = array(
      '#title' => t('Time span for timeline'),
      '#type' => 'select',
      '#options' => $this->timeSpanSelector->getOptions(),
      '#default_value' => $this->options['timespan'],
    );
  }

  /**
   * @param array $result
   *   A Views result array.
   * @param $datefield_alias
   *   The alias for the date column on which the timeline is built
   *
   * @return void
   *   Does not return, but modified $this->view->result.
   */
  protected function trimResult($result, $datefield_alias) {
    if (!isset($datefield_alias)) {
      $this->view->result = array();
      drupal_set_message(t('You must select a date field or property to build a timeline'), 'error');
      return;
    }

    $trimmedResult = $this->timeSpanSelector->trimValues($this->options['timespan'], $this->view->result, $datefield_alias);
    $cropping = count($result) - count($trimmedResult);
    if ($cropping) {
      $this->view->result = $trimmedResult;
      drupal_set_message(t('@count results fall out of the timeline and have been cropped. You should adjust the date filter on the view so that it matches the Timeline time span', array(
        '@count' => $cropping,
      )), 'warning');
    }
  }

  /**
   * @return string|null
   *   The alias for the date column on which the timeline is built, or null if
   *   no column has been selected, preventing the timeline from being built.
   */
  protected function getDateFieldAlias() {
    $datefield_name = $this->options['datefield'];
    $datefield_handler = $this->display->handler->handlers['field'];
    $datefield_field = $datefield_handler[$datefield_name];
    $datefield_alias = isset($datefield_field->field_alias) ? $datefield_field->field_alias : NULL;
    return $datefield_alias;
  }

  /**
   * @param array $limits
   * @param $timestamp
   *
   * @return float
   */
  protected function getRatio(array $limits, $timestamp) {
    $ret = ($timestamp - $limits[0]) / ($limits[1] - $limits[0]);
    return $ret;
  }

  /**
   *
   * @return string
   */
  public function render() {
    $datefield_alias = $this->getDateFieldAlias();
    $this->trimResult($this->view->result, $datefield_alias);
    $rows = array();

    // Prepare position ratios before rendering
    $limits = $this->timeSpanSelector->getSpanLimits($this->options['timespan']);
    $ratios = array();
    foreach ($this->view->result as $index => $row) {
      $ratios[$index] = $this->getRatio($limits, $row->{$datefield_alias});
    }

    // Make sure fields are rendered
    $this->render_fields($this->view->result);

    // Grouping is not allowed, so render as a record set, not a grouping set.
    if ($this->uses_row_plugin()) {
      foreach ($this->view->result as $index => $row) {
        $this->view->row_index = $index;
        $rows[$index] = $this->row_plugin->render($row);
      }
    }

    // Wrap rows in our positioning spans
    $wrapped_rows = array();
    $current_ratio = $this->getRatio($limits, REQUEST_TIME);
    foreach ($rows as $index => &$row) {
      $class = $ratios[$index] < $current_ratio ? 'past' : 'future';
      $ratio = sprintf('%.2f%%', 100 * $ratios[$index]);
      $wrapped_rows[$index] = "<div class=\"$class\" style=\"left: $ratio\">$row</div>\n";
    }

    $ret = theme($this->theme_functions(),
      array(
        'view' => $this->view,
        'options' => $this->options,
        'grouping_level' => 0,
        'rows' => $wrapped_rows,
        'title' => '',
    ));

    // Rendering done, notify views.
    unset($this->view->row_index);

    return $ret;
  }
}
