<?php
/**
 * @file
 * views_timeline_style_views_timeline.inc
 *
 * Theme hook implementations for Views Timeline
 *
 * @author: Frederic G. MARAND <fgm@osinet.fr>
 *
 * @copyright (c) 2013-2014 Ouest Systèmes Informatiques (OSInet).
 *
 * @license General Public License version 2 or later
 */

/**
 * Implements template_preprocess_views_HOOK().
 *
 * @param $vars
 */
function template_preprocess_views_view_views_timeline(&$vars) {
  $handler = $vars['view']->style_plugin;

  $class = explode(' ', $handler->options['class']);
  $class = array_map('views_clean_css_identifier', $class);

  $wrapper_class = explode(' ', $handler->options['wrapper_class']);
  $wrapper_class = array_map('views_clean_css_identifier', $wrapper_class);

  $vars['wrapper_class'] = implode(' ', $wrapper_class);
  if ($vars['wrapper_class']) {
    $vars['wrapper_prefix'] = '<div class="' . $vars['wrapper_class'] . '">';
    $vars['wrapper_suffix'] = '</div>';
  }
  else {
    $vars['wrapper_prefix'] = '';
    $vars['wrapper_suffix'] = '';
  }

  template_preprocess_views_view_unformatted($vars);
}

