<?php
/**
 * @file
 * views-view-views-timeline.tpl.php
 *
 * Views Timeline template.
 *
 * @author: Frederic G. MARAND <fgm@osinet.fr>
 *
 * @copyright (c) 2013-2014 Ouest Systèmes Informatiques (OSInet).
 *
 * @license General Public License version 2 or later
 */
?>
<?php print $wrapper_prefix; ?>
  <div class="views-timeline">
    <div class="title">
    <?php if (!empty($title)) : ?>
      <h3><?php print $title; ?></h3>
    <?php endif; ?>
    </div>

    <div class="band">
      <?php
      foreach ($rows as $row) {
        echo $row;
      }
      ?>
    </div>
  </div>
<?php print $wrapper_suffix; ?>
