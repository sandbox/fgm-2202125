Views Timeline
==============

Copyright/Licensing
-------------------

- (c) 2013-2014 Ouest Systèmes Informatiques (OSInet)
- Licensed under the General Public License version 2 or later (SPDX: GPL-2.0+).
- Contains example icons from the Lullacons icon set, published by Lullabot Inc
  under the same GPL-2.0+ license:
  http://www.lullabot.com/blog/article/free-gpl-icons-lullacons-pack-1

Features
--------

This module contains a style plugin for Views 3, and an example view showing one
possible way to use it.

The example views expects the "standard" install profile and some content.


Understanding the timeline
--------------------------

The timeline is made of a background area, representing a timespan, which is a
setting of the plugin, on which results are placed.

Available timespans are: current day, current week, current month, and current
year. These are calendar time spans, not sliding windows. i.e. on the 29/08/14,
the current year goes from 01/01/14 to 31/12/14. In the current version, weeks
are assumed to start on Monday.

The result placement depends on the value of the date property or field which
they carry, translated as a ratio within the view timespan. For instance, if
the timeline is configured to use a node creation timestamp, a node created on
Friday 18/07/14 will appear just a bit right of the center of a current-year
timeline (a bit after 01/07), a current-month timeline (a bit after 15/07), or a
current-week timeline (friday is just after thursday).


Formatting the timeline
-----------------------

In most cases, you will want to used fields mode, exclude all actual fields from
display, and use a global field building an img element using tokens from the
actual fields, probably including a "title" attribute to show on hovering.

Styling help is provided by the plugin: results timed before the current
timestamp are wrapped in a container with a "past" class, and events timed after
it are wrapped in a container with a "future" class: this allows an easy
visual split between past and future events, with just CSS rules.
