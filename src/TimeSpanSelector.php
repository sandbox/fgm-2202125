<?php
/**
 * @file
 * TimeSpanSelector.php
 *
 * @author: Frederic G. MARAND <fgm@osinet.fr>
 *
 * @copyright (c) 2013-2014 Ouest Systèmes Informatiques (OSInet).
 *
 * @license General Public License version 2 or later
 */

namespace OSInet\ViewsTimeline;

/**
 * Helper class to work with a time span around a reference timestamp.
 *
 * @package OSInet\ViewsTimeline
 */
class TimeSpanSelector {
  const DATE_FORMAT = 'Y/m/d';

  /**
   * @var \DateTime
   */
  public $referenceTimestamp;

  /**
   * @param $referenceTimestamp
   *   The timestamp around which the time span is built.
   */
  public function __construct($referenceTimestamp) {
    $this->referenceTimestamp = $referenceTimestamp;
  }

  /**
   * @return array
   *   Available time spans, usable as a FormAPI options array for a select
   *   element.
   */
  public function getOptions() {
    $ret = array(
      'day' => t('Day (currently: @span)', array('@span' => $this->renderSpanLimits('day'))),
      'week' => t('Week (currently: @span)', array('@span' => $this->renderSpanLimits('week'))),
      'month' => t('Month (currently: @span)', array('@span' => $this->renderSpanLimits('month'))),
      'year' => t('Year (currently: @span)', array('@span' => $this->renderSpanLimits('year')))
    );

    return $ret;
  }

  /**
   * @return string
   *   The default timespan.
   */
  public static function getOptionsDefault() {
    return 'year';
  }

  /**
   * @param $span
   *   A span code
   *
   * @return null|string
   *   A start-end string. Remove 24 hours from the upper limit for date
   *   readability: end timestamp is on 00:00:00 the NEXT day, but humans expect
   *   to see it as if it were the last day on 24:00:00,
   */
  public function renderSpanLimits($span) {
    $limits = $this->getSpanLimits($span);
    if ($span === 'day') {
      $ret = format_date($limits[0], 'custom', static::DATE_FORMAT);
    }
    else {
      $ret = t('@start-@end', array(
        '@start' => format_date($limits[0], 'custom', static::DATE_FORMAT),
        '@end' => format_date($limits[1] - 86400, 'custom', static::DATE_FORMAT),
      ));
    }

    return $ret;
  }

  /**
   * @return
   *   An array of two timestamps for the beginning and end of the time span.
   */
  public function getSpanLimits($span) {
    $referenceTimestamp = $this->referenceTimestamp;
    switch ($span) {
      case 'day':
        $ret = array(
          strtotime('midnight today', $referenceTimestamp),
          strtotime('midnight tomorrow', $referenceTimestamp),
        );
        break;

      case 'week':
        // FIXME handle sun/mon as first DoW
        $ret = array(
          strtotime('midnight previous monday', $referenceTimestamp),
          strtotime('midnight next monday', $referenceTimestamp),
        );
        break;

      case 'month';
        $ret = array(
          strtotime('midnight first day of this month', $referenceTimestamp),
          strtotime('midnight first day of next month', $referenceTimestamp),
        );
        break;

      case 'year';
        $ret = array(
          strtotime('midnight first day of january this year', $referenceTimestamp),
          strtotime('midnight first day of january next year', $referenceTimestamp),
        );
        break;
    }

    return $ret;
  }

  /**
   * Remove values outside the chosen time span
   *
   * @param $span
   *   A timespan code: day, week..
   * @param $values
   *
   *   An array of objects containg a $field timestamp value
   * @param $field
   *   A public timestamp property present on all members of $values.
   *
   * @return array
   *   The array, without values outside the chosen span.
   */
  public function trimValues($span, $values, $field) {
    $ret = array();
    $limits = $this->getSpanLimits($span);

    // Foreach loop is MUCH faster than array_filter() with a closure.
    foreach ($values as $key => $value) {
      if (isset($value->$field) && $value->$field >= $limits[0] && $value->$field < $limits[1]) {
        $ret[$key] = $value;
      }
    }

    return $ret;
  }
}